const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const userRoutes = require('./routes/user');
const classeRoutes = require('./routes/classe');
const studentRoutes = require('./routes/student');
const courseRoutes = require('./routes/course');
const noteRoutes = require('./routes/note');
const app = express();

mongoose.connect('mongodb+srv://mongodb2018:mongodb2018@teacher-cluster.maywi.mongodb.net/teacher?retryWrites=true&w=majority',
  { useNewUrlParser: true,useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch((err) => console.log(err));



app.use(bodyParser.json());
app.use('/api/classes', classeRoutes);
app.use('/api/students', studentRoutes);
app.use('/api/courses', courseRoutes);
app.use('/api/notes', noteRoutes);
app.use('/api/auth', userRoutes);

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});



module.exports = app;