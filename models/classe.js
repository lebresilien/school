const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
  name: { type: String, required: true },
  userId: { type: String, required: true }
});

module.exports = mongoose.model('Classe', thingSchema);