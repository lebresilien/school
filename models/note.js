const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
  name: { type: String, required: true },
  studentId: { type: String, required: true },
  userId: { type: String, required: true },
  classeId: { type: String, required: true },
  courseId: { type: String, required: true },
  note: { type: Number, min: 0, max: 20, required: true }
});

module.exports = mongoose.model('Note', thingSchema);