const mongoose = require('mongoose');

const thingSchema = mongoose.Schema({
  name: { type: String, required: true },
  userId: { type: String, required: true },
  classeId: { type: String, required: true },
});

module.exports = mongoose.model('Student', thingSchema);