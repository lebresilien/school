const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const studentCtrl = require('../controllers/student');

router.get('/', auth, studentCtrl.getAllStudent);
router.post('/', auth, studentCtrl.createStudent);
router.get('/classes/:classeId/users/:userId', auth, studentCtrl.getUserStudent);
//router.put('/:id', auth, classeCtrl.modifyThing);
router.delete('/:id', auth, studentCtrl.deleteStudent);

module.exports = router; 