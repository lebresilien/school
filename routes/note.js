const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const noteCtrl = require('../controllers/note');


router.post('/', auth, noteCtrl.createNote);
router.delete('/:id', auth, noteCtrl.deleteNote);

module.exports = router; 