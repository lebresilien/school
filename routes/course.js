const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const courseCtrl = require('../controllers/course');

router.get('/', auth, courseCtrl.getAllCourse);
router.post('/', auth, courseCtrl.createCourse);
router.get('/classes/:classeId/users/:userId', auth, courseCtrl.getUserCourse);
router.delete('/:id', auth, courseCtrl.deleteCourse);

module.exports = router; 