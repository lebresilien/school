const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const classeCtrl = require('../controllers/classe');

router.get('/', auth, classeCtrl.getAllClasse);
router.post('/', auth, classeCtrl.createClasse);
router.get('/users/:id', auth, classeCtrl.getUserClasse);
//router.put('/:id', auth, classeCtrl.modifyThing);
router.delete('/:id', auth, classeCtrl.deleteClasse);

module.exports = router; 