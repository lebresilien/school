const Classe = require('../models/classe');
const jwt = require('jsonwebtoken');

exports.createClasse = (req, res, next) => {

    const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
    const userId = decodedToken.userId;

    const classe = new Classe({
    name: req.body.name,
    userId: userId
  });
  classe.save().then(
    () => {
      res.status(201).json({
        message: 'Classe crée avec succés!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteClasse = (req, res, next) => {
    
    /* const token = req.headers.authorization.split(' ')[1];
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
    const userId = decodedToken.userId; */

    Classe.deleteOne({_id: req.params.id}).then(
    () => {
      res.status(200).json({
        message: 'Deleted!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getUserClasse = (req, res, next) => {
  Classe.find({ userId: req.params.id }).exec().then(
    (classe) => {
      res.status(200).json(classe);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllClasse = (req, res, next) => {
  Classe.find().then(
    (classe) => {
      res.status(200).json(classe);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};
