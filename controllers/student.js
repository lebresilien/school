const Student = require('../models/student');
const jwt = require('jsonwebtoken');

exports.createStudent = (req, res, next) => {

  const token = req.headers.authorization.split(' ')[1];
  const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
  const userId = decodedToken.userId;

  const student = new Student({
    name: req.body.name,
    userId: userId,
    classeId: req.body.classeId
  });
  student.save().then(
    () => {
      res.status(201).json({
        message: 'Elévé ajouté avec succés!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteStudent = (req, res, next) => {
    Student.deleteOne({_id: req.params.id}).then(
    () => {
      res.status(200).json({
        message: 'Deleted!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};


exports.getUserStudent = (req, res, next) => {
  Student.find({ userId: req.params.userId, classeId: req.params.classeId }).exec().then(
    (student) => {
      res.status(200).json(student);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllStudent = (req, res, next) => {
  Student.find().then(
    (student) => {
      res.status(200).json(student);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};