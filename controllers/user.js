const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const Mailgen = require("mailgen");
const EMAIL_USER = "lebresilien@gmail.com"

let transporter = nodemailer.createTransport({
  host: "smtp.mailtrap.io",
  port: 2525,
  auth: {
    user: "2c6266277de0fe",
    pass: "f6da6b353d2117"
  }
});

let MailGenerator = new Mailgen({
  theme: "default",
  product: {
    name: "TEACHER'S",
    link: "localhost:3000",
    copyright: '© 2021 teacher\'s. tous droits reservés.',
  },
});

exports.signup = (req, res, next) => {
    const { email, name } = req.body;
    bcrypt.hash(req.body.password, 10)
      .then(hash => {
        const user = new User({
          name: req.body.name,
          phone: req.body.phone,
          email: req.body.email,
          password: hash
        });
        user.save()
          //.then(() => res.status(201).json({ message: 'Utilisateur créé !' }))
          .then(() => {

            let response = {
              body: {
                greeting: 'Cher',
                signature: 'Cordialement,',
                name,
                intro: "Bienvenu et merci d'avoir créer votre compte.",
              },
            };

            let mail = MailGenerator.generate(response);

            let message = {
              from: EMAIL_USER,
              to: email,
              subject: "Confirmation creation compte",
              html: mail,
            }

            transporter.sendMail(message)
            .then(() => {
              return res.status(200).json({ msg: "vous recevrez un mail de confirmation" });
            })
            .catch((error) => console.error(error));
            
          })
          .catch(error => res.status(400).json({ error }));
      })
      .catch(error => res.status(500).json({ error }));
};  



exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          return res.status(401).json({ error: 'Utilisateur non trouvé !' });
        }
        bcrypt.compare(req.body.password, user.password)
          .then(valid => {
            if (!valid) {
              return res.status(401).json({ error: 'Mot de passe incorrect !' });
            }
            res.status(200).json({
              userId: user._id, userName: user.name,
              userPhone: user.phone,  userEmail: user.email,
              token: jwt.sign(
                { userId: user._id },
                'RANDOM_TOKEN_SECRET',
                { expiresIn: '24h' }
              )
            });
          })
          .catch(error => res.status(500).json({ error }));
      })
      .catch(error => res.status(500).json({ error }));
  };