const Note = require('../models/note');
const jwt = require('jsonwebtoken');

exports.createNote = (req, res, next) => {

  const token = req.headers.authorization.split(' ')[1];
  const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
  const userId = decodedToken.userId;
  const note = new Note({
    name: req.body.name,
    studentId: req.body.studentId,
    userId: userId,
    classeId: req.body.classeId,
    courseId: req.body.courseId,
    note: req.body.note
  });
  note.save().then(
    () => {
      res.status(201).json({
        message: 'Note ajoutée avec succés!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteNote = (req, res, next) => {
    Note.deleteOne({_id: req.params.id}).then(
    () => {
      res.status(200).json({
        message: 'Deleted!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};


/* exports.getUserNote = (req, res, next) => {
  Student.find({ userId: req.params.userId, classeId: req.params.classeId }).exec().then(
    (student) => {
      res.status(200).json(student);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
}; */

/* exports.getAllStudent = (req, res, next) => {
  Student.find().then(
    (student) => {
      res.status(200).json(student);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
}; */