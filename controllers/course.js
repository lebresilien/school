const Course = require('../models/course');
const jwt = require('jsonwebtoken');

exports.createCourse = (req, res, next) => {

  const token = req.headers.authorization.split(' ')[1];
  const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
  const userId = decodedToken.userId;

  const course = new Course({
    name: req.body.name,
    userId: userId,
    classeId: req.body.classeId,
  });
  course.save().then(
    () => {
      res.status(201).json({
        message: 'Cours crée avec succés!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.deleteCourse= (req, res, next) => {
  Course.deleteOne({_id: req.params.id}).then(
    () => {
      res.status(200).json({
        message: 'Deleted!'
      });
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getUserCourse = (req, res, next) => {
  Course.find({ userId: req.params.userId, classeId: req.params.classeId }).exec().then(
    (course) => {
      res.status(200).json(course);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllCourse = (req, res, next) => {
  Course.find().then(
    (course) => {
      res.status(200).json(course);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};